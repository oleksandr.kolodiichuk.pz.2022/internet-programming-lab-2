class Student
{
    constructor(id, group, name, gender, birthday, status)
    {
        this.id = id;
        this.group = group;
        this.name = name;
        this.gender = gender;
        this.birthday = birthday;
        this.status = status;
    }
}

var students = [];
var studentId = 1;

function ShowFormAddStudent()
{
    $("#firstnameId").val("");
    $("#secondnameId").val("");
    $("#idSelectGroup").val("");
    $("#idSelectGender").val("");
    $("[name='nameDateBirthday']").val("");

    var FormAddStudent = $("#FormAddStudent");
    if (FormAddStudent.css("display") === "none")
    {
        FormAddStudent.css("display", "block");
        var h4 = FormAddStudent.find("h4");
        h4.text("Add student");
        var btnEdit = $("#btnCreateEdit")
        btnEdit.text("Create");
    }
    else
    {
        FormAddStudent.css("display", "none");
    }
}

function ShowFormEditStudent(id)
{
    var FormEditStudent = $("#FormAddStudent");
    if (FormEditStudent.css("display") === "none")
    {
        FormEditStudent.css("display", "block");
        var h4 = FormEditStudent.find("h4");
        h4.text("Edit student");
        var btnEdit = $("#btnCreateEdit");
        btnEdit.text("Save");
        btnEdit.attr("data-id", id);

        var student = students.find(student => student.id === id);
        $("#idSelectGroup").val(student.group);
        var names = student.name.split(" ");
        $("#firstnameId").val(names[0]);
        $("#secondnameId").val(names[1]);
        $("#idSelectGender").val(student.gender);
        $("[name='nameDateBirthday']").val(student.birthday);
    }
    else
    {
        FormEditStudent.css("display", "none");
    }
}

function addEditStudent()
{
    var FormEditStudent = $("#FormAddStudent");
    var h4 = FormEditStudent.find("h4");
    var btnCreateEdit = $("#btnCreateEdit");

    if (h4.text() == "Add student" && btnCreateEdit.text() == "Create")
    {
        var firstName = $("#firstnameId").val();
        var secondName = $("#secondnameId").val();
        var group = $("#idSelectGroup").val();
        var gender = $("#idSelectGender").val();
        var birthday = $("[name='nameDateBirthday']").val();

        var birthYear = new Date(birthday).getFullYear();
        if (birthYear >= 2008)
        {
            alert("Student must be born before 2008!");
            return false;
        }

        if(!(isValidName(firstName)))
        {
            alert("Only digits must be in field for firstname");
            return;
        }
        if(!(isValidName(secondName)))
        {
            alert("Only digits must be in field for secondname");
            return;
        }
        
        const maxNumberOfRows = 5;

        if (firstName !== "" && secondName !== "" && group !== "" && gender !== "" && birthday !== "")
        {
            var tableBody = $("#tableId tbody");
            if (tableBody.find('tr').length < maxNumberOfRows)
            {
                var newRow = $("<tr>");
                var newCell1 = $("<td>").html('<input class="Checkboxes" type="checkbox" name="Checkbox' + studentId + '">');
                var newCell2 = $("<td>").text(group);
                var newCell3 = $("<td>").text(firstName + " " + secondName);
                var newCell4 = $("<td>").text(gender);
                var newCell5 = $("<td>").text(birthday);
                var newCell6 = $("<td>").html('<input type="radio" name="Status" disabled style="scale: 1.5;">');
                var newCell7 = $("<td>").html('<button id="btnEdit' + studentId + '" onclick="ShowFormEditStudent(' + studentId + ')" class="Buttons">&#128394;</button><button onclick="deleteRowInTable(this)" class="Buttons">&#10060;</button>');

                newRow.append(newCell1, newCell2, newCell3, newCell4, newCell5, newCell6, newCell7);
                tableBody.append(newRow);

                var newStudent = new Student(studentId, group, firstName + " " + secondName, gender, birthday, "inactive");
                students.push(newStudent);
                studentId++;
                console.log("Added new Student №" + (studentId - 1));
                console.log(group);
                console.log(firstName + " " + secondName);
                console.log(gender);
                console.log(birthday);
                console.log("\n");
            }
            else
            {
                alert("You reached max number of table's rows (5)!");
            }
        }
        else
        {
            alert("Input all fields!!!");
            return;
        }
        $("#firstnameId").val("");
        $("#secondnameId").val("");
        $("#idSelectGroup").val("");
        $("#idSelectGender").val("");
        $("[name='nameDateBirthday']").val("");
        sendDataToServer();
    }
    else if (h4.text() == "Edit student" && btnCreateEdit.text() == "Save")
    {
        var editedId = parseInt($("#btnCreateEdit").attr("data-id"));
        var editedStudent = students.find(student => student.id === editedId);

        editedStudent.group = $("#idSelectGroup").val();
        editedStudent.name = $("#firstnameId").val() + " " + $("#secondnameId").val();
        editedStudent.gender = $("#idSelectGender").val();
        editedStudent.birthday = $("[name='nameDateBirthday']").val();

        if(!isValidName($("#firstnameId").val()))
        {
            alert("Name must not contain digits!");
            return;
        }
        if(!isValidName($("#secondnameId").val()))
        {
            alert("Surname must not contain digits!");
            return;
        }
        var birthYear = new Date(editedStudent.birthday).getFullYear();
        if (birthYear >= 2008)
        {
            alert("Student must be born before 2008!");
            return false;
        }

        console.log("Edited Student №" + editedId);
        console.log(editedStudent.group);
        console.log(editedStudent.name);
        console.log(editedStudent.gender);
        console.log(editedStudent.birthday);
        console.log("\n");

        var editedRow = $("#tableId tbody tr").find(`button[id^="btnEdit${editedId}"]`).closest("tr");
        editedRow.find("td:eq(1)").text(editedStudent.group);
        editedRow.find("td:eq(2)").text(editedStudent.name);
        editedRow.find("td:eq(3)").text(editedStudent.gender);
        editedRow.find("td:eq(4)").text(editedStudent.birthday);
        FormEditStudent.css("display", "none");
        sendDataToServer();
    }
}

function isValidName(name)
{
    for (var i = 0; i < name.length; i++)
    {
        var charCode = name.charCodeAt(i);
        if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122))
        {
            return false;
        }
    }
    return true;
}

function deleteRowInTable(button)
{
    var row = $(button).closest('tr');
    var studentId = parseInt(row.find('button[id^="btnEdit"]').attr("id").replace("btnEdit", ""));
    var studentIndex = students.findIndex(student => student.id === studentId);
    students.splice(studentIndex, 1);
    row.remove();
    console.log("Deleted Student №" + studentId);
    console.log("\n");
    sendDataToServer();

    //Оновлення індексів кнопок редагування для решти студентів
    $("#tableId tbody tr").each(function(index)
    {
        var currentStudentId = parseInt($(this).find('button[id^="btnEdit"]').attr("id").replace("btnEdit", ""));
        $(this).find('button[id^="btnEdit"]').attr("onclick", `ShowFormEditStudent(${currentStudentId})`);
    });

    //Оновлення індексів для кнопок видалення
    $("#tableId tbody tr").each(function(index)
    {
        var currentStudentId = parseInt($(this).find('button[id^="btnEdit"]').attr("id").replace("btnEdit", ""));
        $(this).find('button[id^="btnDelete"]').attr("onclick", `deleteRowInTable(this)`);

        //Оновлення індексів студентів в атрибуті data-student-index для всіх рядків таблиці після видалення
        $("#tableId tbody tr").each(function(index)
        {
            var currentStudentId = parseInt($(this).find('button[id^="btnEdit"]').attr("id").replace("btnEdit", ""));
            $(this).attr("data-student-index", currentStudentId);
        });
    });
}

//Відправлення даних про студентів на сервер за допомогою HTTP-запиту типу POST
function sendDataToServer()
{
    const studentsJSON = JSON.stringify(students); //Конвертація об'єкта в JSON
    console.log(studentsJSON);
    $.ajax({
        type: "POST",
        url: "https://jsonplaceholder.typicode.com/posts",
        data: studentsJSON,
        success: function(response) {
            console.log("Sent.");
        },
        error: function(xhr, status, error) {
            console.error("Failed. Error :", error);
        },
    });
}